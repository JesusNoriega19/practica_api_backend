const express = require("express");
const modelSucursalNuevo = require("../../Models/sucursal/modelNuevoSucursal");

let app = express();

//!Agregar

app.post("/sucursal/nuevo", (req, res) => {
  let body = req.body;
  console.log(body);

  let newSchemaSucursal = new modelSucursalNuevo({
    nombreSucursal: body.nombreSucursal,
    direccionSucursal: body.direccionSucursal,
    telefonoSucursal: body.telefonoSucursal,
    gerenteSucursal: body.gerenteSucursal,
    horarioAtencionSucursal: body.horarioAtencionSucursal,
  });

  newSchemaSucursal
    .save()
    .then((data) => {
      return res.status(200).json({
        ok: true,
        message: "SE GUARDARON LOS DATOS",
        data,
      });
    })

    .catch((err) => {
      return res.status(200).json({
        ok: false,
        message: "NO SE GUARDARON LOS DATOS",
        err,
      });
    });
});

//! Mostrar

app.get("/obtener/datos/sucursal", async (req, res) => {
  const respuesta = await modelSucursalNuevo.find();
  res.status(200).json({
    ok: true,
    respuesta,
  });
});

//! Actualizar

app.put("/actualizar/registro/sucursal/:id", async (req, res) => {
  let id = req.params.id;
  const campos = req.body;

  // elminar campos campos que no queramos actualizar

  // delete campos.nombrePersonal
  // delete campos.cargoPersonal
  // delete campos.contactoPersonal
  // delete campos.fechadecontratoPersonal
  // delete campos.horariolaboralPersonal

  const respuesta = await modelSucursalNuevo.findByIdAndUpdate(id, campos, {
    new: true,
  });
  res.status(202).json({
    ok: true,
    msj: "DATOS ACTUALIZADOS CORRECTAMENTE",
    respuesta,
  });
});

//!Eliminar

app.delete("/eliminar/registro/sucursal/:id", async (req, res) => {
  let id = req.params.id;
  const respuesta = await modelSucursalNuevo.findByIdAndDelete(id);

  res.status(200).json({
    ok: true,
    msj: "REGISTRO ELIMINADO CORRECTAMENTE",
    respuesta,
  });
});

module.exports = app;
