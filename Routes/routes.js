//Contendra los metodos GET, POST, DELETE, UPDATE

const express = require ('express');

const app = express ();

app.use (require('./test'));
app.use (require('./productoNuevo/routeNuevoProducto'));
app.use (require('./personalNuevo/routeNuevoPersonal'));
app.use (require('./proveedorNuevo/routeNuevoProveedor'));
app.use (require('./sucursalNueva/routeNuevoSucursal'));


module.exports = app;