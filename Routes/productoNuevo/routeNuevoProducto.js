const express = require('express');
const modelProductoNuevo = require('../../Models/refaccionaria/modelNuevoProducto');

let app = express();

//!Agregar

app.post('/producto/nuevo',(req,res)=>{
let body = req.body;
console.log(body);


let newSchemaProducto = new modelProductoNuevo({
    nombreProducto : body.nombreProducto,
    marcaProducto : body.marcaProducto,
    presentacionProducto : body.presentacionProducto,
    contenidoProducto : body.contenidoProducto,
    costoProducto : body.costoProducto,
    provedorProducto : body.provedorProducto,
    cantidadIngresa : body.cantidadIngresa,
    statusProducto : body.statusProducto,
    descripcionProducto : body.descripcionProducto,
    });


newSchemaProducto
.save()
.then(
    (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:'SE GUARDARON LOS DATOS',
            data
        });
    })


.catch (
    (err)=>{
        return res.status(200)
        .json({
            ok:false,
            message:'NO SE GUARDARON LOS DATOS',
            err
        });
    })

});


//! Mostrar

app.get('/obtener/datos/producto', async (req, res)=>{

    const respuesta = await modelProductoNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });

})




//! Actualizar 

app.put('/actualizar/registro/:id', async(req,res)=>{
    let id = req.params.id;
    const campos = req.body;

    // elminar campos campos que no queramos actualizar

        // delete campos.nombreProducto
        // delete campos.nombreProducto
        // delete campos.marcaProducto 
        // delete campos.presentacionProducto 
        // delete campos.contenidoProducto 
        // delete campos.costoProducto 
        // delete campos.provedorProducto 
        // delete campos.cantidadIngresa
        // delete campos.statusProducto 
        // delete campos.descripcionProducto 


    const respuesta = await modelProductoNuevo.findByIdAndUpdate(id,campos,{new:true});
    res.status(202).json({
        ok:true,
        msj:"DOCUMENTO ACTUALIZADO CORRECTAMENTE",
        respuesta
    });
});


//!Eliminar

app.delete('/eliminar/registro/:id', async (req, res)=>{
    let id = req.params.id;
    const respuesta = await modelProductoNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj: "REGISTRO FUE ELIMINADO CORRECTAMENTE",
        respuesta
    });
});





module.exports = app;