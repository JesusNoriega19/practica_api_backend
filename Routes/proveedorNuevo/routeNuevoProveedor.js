const express = require('express');
const modelProveedorNuevo = require('../../Models/proveedor/modelNuevoProveedor');

let app = express();

//!Agregar

app.post('/proveedor/nuevo',(req,res)=>{
let body = req.body;
console.log(body);


let newSchemaProveedor = new modelProveedorNuevo({
    nombreProveedor : body.nombreProveedor,
    contactoProveedor : body.contactoProveedor,
    ProductosSuministrados : body.ProductosSuministrados,
    condicionPago : body.condicionPago,
    evaluacionProveedor : body.evaluacionProveedor,
    });


newSchemaProveedor
.save()
.then(
    (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:'SE GUARDARON LOS DATOS',
            data
        });
    })


.catch (
    (err)=>{
        return res.status(200)
        .json({
            ok:false,
            message:'NO SE GUARDARON LOS DATOS',
            err
        });
    })

});


//! Mostrar

app.get('/obtener/datos/proveedor', async (req, res)=>{

    const respuesta = await modelProveedorNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });

})


//! Actualizar 

app.put('/actualizar/registro/proveedor/:id', async(req,res)=>{
    let id = req.params.id;
    const campos = req.body;

    // elminar campos campos que no queramos actualizar

        // delete campos.nombreProveedor
        // delete campos.contactoProveedor
        // delete campos.ProductosSuministrados 
        // delete campos.condicionPago 
        // delete campos.evaluacionProveedor 


    const respuesta = await modelProveedorNuevo.findByIdAndUpdate(id,campos,{new:true});
    res.status(202).json({
        ok:true,
        msj:"DATOS ACTUALIZADOS CORRECTAMENTE",
        respuesta
    });
});


//!Eliminar

app.delete('/eliminar/registro/proveedor/:id', async (req, res)=>{
    let id = req.params.id;
    const respuesta = await modelProveedorNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj: "REGISTRO ELIMINADO CORRECTAMENTE",
        respuesta
    });
});


module.exports = app;