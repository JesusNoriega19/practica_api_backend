const express = require('express');
const modelPersonalNuevo = require('../../Models/personal/modelNuevoPersonal');

let app = express();

//!Agregar

app.post('/personal/nuevo',(req,res)=>{
let body = req.body;
console.log(body);


let newSchemaPersonal = new modelPersonalNuevo({
    nombrePersonal : body.nombrePersonal,
    cargoPersonal : body.cargoPersonal,
    contactoPersonal : body.contactoPersonal,
    fechadecontratoPersonal : body.fechadecontratoPersonal,
    horariolaboralPersonal : body.horariolaboralPersonal,
    });


newSchemaPersonal
.save()
.then(
    (data)=>{
        return res.status(200)
        .json({
            ok:true,
            message:'SE GUARDARON LOS DATOS',
            data
        });
    })


.catch (
    (err)=>{
        return res.status(200)
        .json({
            ok:false,
            message:'NO SE GUARDARON LOS DATOS',
            err
        });
    })

});


//! Mostrar

app.get('/obtener/datos/personal', async (req, res)=>{

    const respuesta = await modelPersonalNuevo.find();
    res.status(200).json({
        ok:true,
        respuesta
    });

})




//! Actualizar 

app.put('/actualizar/registro/personal/:id', async(req,res)=>{
    let id = req.params.id;
    const campos = req.body;

    // elminar campos campos que no queramos actualizar

        // delete campos.nombrePersonal
        // delete campos.cargoPersonal
        // delete campos.contactoPersonal 
        // delete campos.fechadecontratoPersonal 
        // delete campos.horariolaboralPersonal 


    const respuesta = await modelPersonalNuevo.findByIdAndUpdate(id,campos,{new:true});
    res.status(202).json({
        ok:true,
        msj:"DATOS ACTUALIZADOS CORRECTAMENTE",
        respuesta
    });
});


//!Eliminar

app.delete('/eliminar/registro/personal/:id', async (req, res)=>{
    let id = req.params.id;
    const respuesta = await modelPersonalNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok:true,
        msj: "REGISTRO ELIMINADO CORRECTAMENTE",
        respuesta
    });
});





module.exports = app;