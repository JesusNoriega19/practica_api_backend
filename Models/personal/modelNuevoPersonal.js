const mongoose = require ('mongoose');
let Schema = mongoose.Schema;

let nuevoPersonalRefaccionaria = new Schema ({
    nombrePersonal : {type:String},
    cargoPersonal : {type:String},
    contactoPersonal : {type:String},
    fechadecontratoPersonal : {type:String},
    horariolaboralPersonal : {type:String},
});

module.exports = mongoose.model('nuevoPersonal',nuevoPersonalRefaccionaria);

