const mongoose = require ('mongoose');
let Schema = mongoose.Schema;

let nuevoSucursalRefaccionaria = new Schema ({
    nombreSucursal : {type:String},
    direccionSucursal : {type:String},
    telefonoSucursal : {type:String},
    gerenteSucursal : {type:String},
    horarioAtencionSucursal : {type:String},
});

module.exports = mongoose.model('nuevoSucursal',nuevoSucursalRefaccionaria);

