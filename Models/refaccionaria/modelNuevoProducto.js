const mongoose = require ('mongoose');
let Schema = mongoose.Schema;

let nuevoProductoRefaccionaria = new Schema ({
    nombreProducto : {type:String},
    marcaProducto : {type:String},
    presentacionProducto : {type:String},
    contenidoProducto : {type:String},
    costoProducto : {type:Number},
    provedorProducto : {type:String},
    cantidadIngresa : {type:Number},
    statusProducto : {type:String},
    descripcionProducto : {type:String},
});

module.exports = mongoose.model('nuevoProducto',nuevoProductoRefaccionaria);

