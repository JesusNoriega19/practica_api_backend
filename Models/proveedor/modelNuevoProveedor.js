const mongoose = require ('mongoose');
let Schema = mongoose.Schema;

let nuevoProveedorRefaccionaria = new Schema ({
    nombreProveedor : {type:String},
    contactoProveedor : {type:String},
    ProductosSuministrados : {type:String},
    condicionPago : {type:String},
    evaluacionProveedor : {type:String},
});

module.exports = mongoose.model('nuevoProveedor',nuevoProveedorRefaccionaria);

