//Configuraciones, guardara las clases y constantes de los clusters en la nube, entre otras cosas.

"use strict";

const mongoose = require("mongoose");
const app = require("../Server/index");
const port = 3900;

// promesa global
mongoose.Promise = global.Promise;

// hacer la conexion a la bd
mongoose.connect("mongodb://localhost:27017/refaccionaria", {
    useNewUrlParser: true,
  })

  .then(() => {
    console.log("base de datos corriendo");

    //escuchar el puerto
    app.listen(port, () => {
      console.log(`Server corriendoe en puerto: ${port}`);
    });
  });

